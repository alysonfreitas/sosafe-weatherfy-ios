//
//  WeatherViewController.m
//  Weatherfy
//
//  Created by Eduardo Toledo on 12/13/17.
//  Copyright © 2017 SoSafe. All rights reserved.
//

#import "WeatherViewController.h"
#import "Weatherfy-Swift.h"

@import MapKit;

@interface WeatherViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UILabel *varianceLabel;
@end

@implementation WeatherViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)onActionButtonPressed:(UIButton *)sender
{
    [self.textField resignFirstResponder];
    [self updateLabels];
    [self setCenterCoordinate];
}

- (void)updateLabels
{
    DataSource *ds = [DataSource new];
    double mean = [ds meanWithData:DataSource.data in:self.textField.text];
    double variance = [ds varianceWithData:DataSource.data in:self.textField.text];
    self.averageLabel.text = [NSString stringWithFormat:@"%.2f", mean];
    self.varianceLabel.text = [NSString stringWithFormat:@"%.2f", variance];
}

- (void)setCenterCoordinate
{
    NSLog(@"searching: %@", self.textField.text);

    MKLocalSearchRequest *request = [MKLocalSearchRequest new];
    [request setNaturalLanguageQuery:self.textField.text];
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    [search startWithCompletionHandler:
    ^(MKLocalSearchResponse * _Nullable response, NSError * _Nullable error) {
        if(response){
            CLLocationCoordinate2D center = response.boundingRegion.center;
            [self.mapView setCenterCoordinate:center animated:YES];
        }
    }];
}

@end
